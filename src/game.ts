
import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX, spawnTextX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'
import { GroundCover } from './modules/GroundCover'

//import utils from "../node_modules/decentraland-ecs-utils/index"


const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

class RotatorSystem {
  // this group will contain every entity that has a Transform component
  group = engine.getComponentGroup(Transform)

  update(dt: number) {
    // iterate over the entities of the group
    for (let entity of this.group.entities) {
      // get the Transform component of the entity
      const transform = entity.getComponent(Transform)

      // mutate the rotation
      transform.rotate(Vector3.Up(), dt * 10)
    }
  }
}

const mazeShape = new GLTFShape('models/maze/maze.glb')
const maze = spawnGltfX(mazeShape, 8,-0.5,8,  0,89,0,  0.68,1.76,0.72)

/*
------------- BuilderHUD entities -------------
preview.js:8 0,0:  Existing(8,0,8,  0,89,0,  0.68,1.76,0.72)
preview.js:8 0,0:  -------------------------------------------------
preview.js:8 0,0:  --------------- BuilderHUD entities -------------
preview.js:8 0,0:  Existing(8,0,8,  0,89,0,  0.68,1.76,0.72)
const hud:BuilderHUD =  new BuilderHUD()
hud.setDefaultParent(scene)
hud.attachToEntity(maze)
/// --- Spawn a cube ---
*/


const grassyFineTexture = new Texture("materials/Tileable-Textures/redtexture.png")
const grassyFineMaterial = new Material()
grassyFineMaterial.albedoTexture = grassyFineTexture

const ground = new GroundCover (0,0,16,0.01,16,grassyFineMaterial,false)
ground.setParent(scene)